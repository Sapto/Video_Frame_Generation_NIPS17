from glob import glob
import numpy as np
import os
import random
import shutil
from scipy.ndimage import imread

TRAIN_DIR_CLIPS = '../Data/Processed/'
NUM_CLIPS = 5000000  # len(glob(TRAIN_DIR_CLIPS + '*'))


def load_train_sample():
    path = TRAIN_DIR_CLIPS + str(np.random.choice(NUM_CLIPS)) + '.npz'
    clip = np.load(path)['arr_0']

    print clip
    print clip.shape

# load_train_sample()


def load_image(image_path):
    image = imread(image_path, mode = 'RGB')

    print image
    print image.shape

# load_image('../Data/Train/0000/00070.png')


def reverse_sampled_data():
    NUM_CLIPS = len(glob(TRAIN_DIR_CLIPS + '*'))
    TRAIN_DIR_REV_CLIPS = '../Data/Processed_Rev/'
    TRAIN_HEIGHT = 32
    TRAIN_WIDTH = 32
    HIST_LEN = 4

    for i in range(0, NUM_CLIPS):
        path = TRAIN_DIR_CLIPS + str(i) + '.npz'
        clip = np.load(path)['arr_0']

        rev_clip = np.empty([TRAIN_HEIGHT, TRAIN_WIDTH, 3 * (HIST_LEN + 3)])

        for j in range(0, len(clip)):
            for k in range(0, len(clip[j])):
                rev_frame = np.flip(clip[j, k], 0)
                rev_clip[j][k] = rev_frame

        np.savez_compressed(TRAIN_DIR_REV_CLIPS + str(i), rev_clip)

        if (i + 1) % 100 == 0: print 'Processed %d clips' % (i + 1)

# reverse_sampled_data()


def create_train_test():
    output_folder = '../Data/Test_UCF/'
    files = [f for f in os.listdir('/Users/sourish1997/Downloads/frames')]

    random.shuffle(files)
    test_files = files[:len(files) / 10]

    for i in range(len(test_files)):
        shutil.move(os.path.join('/Users/sourish1997/Downloads/frames', test_files[i]), output_folder)

# create_train_test()


def move_files():
    for i in range(0, 300001):
        file_name = str(i) + ".npz"
        shutil.move(os.path.join('../Pacman_Data/', file_name), '../Pacman_Data/Processed/')

# move_files()


def random_test():
    img_path = glob(os.path.join("/home/vplab/Downloads/UCF_Data/Test", '*/*'))[0]
    print img_path


random_test()